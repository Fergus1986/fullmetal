#include "GameOverState.h"

GameOverState::GameOverState()
{
    m_bQuit         = false;
    m_FrameEvent    = Ogre::FrameEvent();
}

void GameOverState::enter()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Entrando en GameOver...");

    m_pSceneMgr = OgreFramework::getSingletonPtr()->m_pRoot->createSceneManager(ST_GENERIC, "GameOverSceneMgr");
    m_pSceneMgr->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));

    m_pCamera = m_pSceneMgr->createCamera("GameOverCam");
    m_pCamera->setPosition(Vector3(0, 25, -50));
    m_pCamera->lookAt(Vector3(0, 0, 0));
    m_pCamera->setNearClipDistance(1);

    m_pCamera->setAspectRatio(Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualWidth()) /
                              Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualHeight()));

    OgreFramework::getSingletonPtr()->m_pViewport->setCamera(m_pCamera);

    //    OgreFramework::getSingletonPtr()->m_pTrayMgr->destroyAllWidgets();
    OgreFramework::getSingletonPtr()->m_pTrayMgr->showCursor();
    OgreFramework::getSingletonPtr()->m_pTrayMgr->createButton(OgreBites::TL_CENTER, "VolverMenu", "Volver al Menu", 500);
    OgreFramework::getSingletonPtr()->m_pTrayMgr->createButton(OgreBites::TL_CENTER, "Salir", "Salir", 500);

    this->_overlayManager = Ogre::OverlayManager::getSingletonPtr();
    fondo = _overlayManager->getByName("Fondo");
    fondo->show();

    tanque = _overlayManager->getByName("Tanque2");
    tanque->show();

    _gameOverOverlay = _overlayManager->getByName("GameOverOverlay");
    _gameOverOverlay->show();

    m_bQuit = false;

    createScene();
    cout << "GameOver Escena creada" << endl;
}

void GameOverState::createScene()
{
}

void GameOverState::exit()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Saliento de GameOver..");

    m_pSceneMgr->destroyCamera(m_pCamera);
    if(m_pSceneMgr)
        OgreFramework::getSingletonPtr()->m_pRoot->destroySceneManager(m_pSceneMgr);

    OgreFramework::getSingletonPtr()->m_pTrayMgr->clearAllTrays();
    OgreFramework::getSingletonPtr()->m_pTrayMgr->destroyAllWidgets();
    OgreFramework::getSingletonPtr()->m_pTrayMgr->setListener(0);

    _gameOverOverlay->hide();
    fondo->hide();
    tanque->hide();
}

bool GameOverState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_ESCAPE))
    {
        cout << "ESC presionado" << endl;
        this->m_bQuit=true;
    }
}

bool GameOverState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
}

bool GameOverState::mouseMoved(const OIS::MouseEvent &evt)
{
    if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseMove(evt)) return true;
    return true;
}

bool GameOverState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseDown(evt, id)) return true;
    return true;
}

bool GameOverState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseUp(evt, id)) return true;
    return true;
}

void GameOverState::buttonHit(OgreBites::Button *button)
{

    if(button->getName() == "Salir"){
        m_bQuit = true;

    }else if(button->getName() == "VolverMenu"){
        cout << "Pulsado boton Menu (GameOver)" << endl;
        this->popAllAndPushAppState(findByName("Menu"));
    }
}

void GameOverState::update(double timeSinceLastFrame)
{
    m_FrameEvent.timeSinceLastFrame = timeSinceLastFrame;
    OgreFramework::getSingletonPtr()->m_pTrayMgr->frameRenderingQueued(m_FrameEvent);

    if(this->m_bQuit){
        shutdown();
    }
}
