#ifndef ACERCASTATE_H
#define ACERCASTATE_H

#include <iostream>
#include <string.h>

#include <OGRE/Ogre.h>

#include "AppState.hpp"


using namespace std;
using namespace Ogre;

class AcercaState: public AppState
{
public:
    AcercaState();
    DECLARE_APPSTATE_CLASS(AcercaState)

    void enter();
    void createScene();
    void exit();

    bool keyPressed(const OIS::KeyEvent &keyEventRef);
    bool keyReleased(const OIS::KeyEvent &keyEventRef);

    bool mouseMoved(const OIS::MouseEvent &evt);
    bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
    bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

    void update(double timeSinceLastFrame);

private:
   bool m_bQuit;

   // Overlays
   Ogre::OverlayManager* _overlayManager;
   Ogre::Overlay *fondo;
   Ogre::Overlay *_acercaOverlay;
   Ogre::Overlay *_textoAcerca;
};

#endif // ACERCASTATE_H
