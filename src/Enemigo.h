#ifndef ENEMIGO_H
#define ENEMIGO_H

#define DELTA 0.1

#define PARADO      0
#define ADELANTE    1
#define GIRAR       2
#define ATRAS       3

#include <iostream>
#include <string>
#include <math.h>

#include "PhysicsManager.hpp"

#include <OGRE/Ogre.h>

using namespace std;

class Enemigo
{
public:
    Enemigo(string nombre,
            btVector3 posicionInicial, btVector3 posicionFinal,
            SceneNode *escenario,
            SceneManager *sceneMgr,
            PhysicsManager *mPhysics);

    ~Enemigo();

    SceneNode* getNodo();
    btRigidBody * getCuerpo();
    void actualizarPosicion();
    void moverAdelante();
    void girar();
    void moverAtras();
//    void destruir();


private:

    PhysicsManager *_physics;
    SceneManager* _sceneMgr;

    string _nombre;

    btVector3 _posicionInicial;
    btVector3 _posicionFinal;
    btVector3 _posicionActual;
    btQuaternion _rotacion;
    int _estado;

    // Grafico
    Entity* _entidad;
    SceneNode* _nodo;

    // Fisica
    btTransform _transformacion;
    btConvexShape * _colision;
    btRigidBody * _cuerpo;
};

#endif // ENEMIGO_H
