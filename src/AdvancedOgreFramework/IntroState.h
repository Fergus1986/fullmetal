#ifndef INTROSTATE_H
#define INTROSTATE_H

#define TIEMPO_INTRO 19

#include <iostream>

#include <OGRE/Ogre.h>

#include "AppState.hpp"

#include "Escenario.h"
#include "PhysicsManager.hpp"

#include "TrackManager.hpp"
#include "SoundFXManager.hpp"


using namespace std;
using namespace Ogre;



class IntroState: public AppState
{
public:
    IntroState();
    DECLARE_APPSTATE_CLASS(IntroState)

    void enter();
    void createScene();
    void exit();

    bool keyPressed(const OIS::KeyEvent &keyEventRef);
    bool keyReleased(const OIS::KeyEvent &keyEventRef);

    bool mouseMoved(const OIS::MouseEvent &evt);
    bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
    bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

    void buttonHit(OgreBites::Button* button);

    void update(double timeSinceLastFrame);

private:
    bool m_bQuit;
    Ogre::OverlayManager* _overlayManager;
    Ogre::Overlay *_intro;

    Entity* _entidadJugador;
    SceneNode* _nodoJugador;

    // Escenario
    Escenario* _escenario;

    // Fisica
    PhysicsManager* 			mPhysics;
    DebugDrawer*				mDebugDrawer;
    bool mEnabledPhysicsDebugDraw;

    // Tiempo
    double _tiempo;
    int _segundos;

    // Musica
    TrackManager*				_trackMgr;
    SoundFXManager*				_soundFxMgr;
    TrackPtr 					_musicTrack;



};

#endif // INTROSTATE_H
