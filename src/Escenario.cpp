#include "Escenario.h"

Escenario::Escenario(string nombre, SceneManager *sceneManager, PhysicsManager *mPhysics)
{
    this->_nombre=nombre;
    this->_nodoEscenario=sceneManager->getRootSceneNode()->createChildSceneNode(this->_nombre);
    this->_sceneManager = sceneManager;

    this->_mPhysics = mPhysics;

    this->_ancho = 0;
    this->_alto = 0;

    // Leer archivo
    this->leerArchivo();

    cout << "ancho: " << _ancho << " alto: " << _alto << endl;
    cout << "nCasillas: " << this->_tipoCasillas.size() << endl;
    cout << "tamano Mapa: " << this->_indiceCasillas.size() << endl;

    this->crearEscenario();
    this->crearEnemigos();
}

Escenario::~Escenario()
{

    for(int i=0; i<_escenario.size(); i++){
        delete _escenario.at(i);
    }
    cout << "casillas destruidas" << endl;

    for(int i=0; i<_enemigos.size(); i++){
        delete _enemigos.at(i);
    }
    cout << "enemigos destruidos" << endl;

    _sceneManager->getRootSceneNode()->removeChild(_nodoEscenario);
    _mPhysics->getDynamicsWorld()->removeRigidBody(_cuerpo);

}


void Escenario::leerArchivo()
{
    cout << "Leyendo archivo " << this->_nombre << endl;
    // Inicializacion
    try {
        XMLPlatformUtils::Initialize();
    }
    catch (const XMLException& toCatch) {
        char* message = XMLString::transcode(toCatch.getMessage());
        cout << "Error durante la inicialización! :\n"
             << message << "\n";
        XMLString::release(&message);
        return;
    }

    XercesDOMParser* parser = new XercesDOMParser();
    parser->setValidationScheme(XercesDOMParser::Val_Always);

    // 'Parseando' el fichero xml...
    try {
        parser->parse(this->_nombre.c_str());
    }
    catch (const XMLException& toCatch) {
        char* message = XMLString::transcode(toCatch.getMessage());
        cout << "Excepción capturada: \n"
             << message << "\n";
        XMLString::release(&message);
    }
    catch (const DOMException& toCatch) {
        char* message = XMLString::transcode(toCatch.msg);
        cout << "Excepción capturada: \n"
             << message << "\n";
        XMLString::release(&message);
    }
    catch (...) {
        cout << "Excepción no esperada.\n" ;
        return;
    }

    DOMDocument* xmlDoc;
    DOMElement* elementRoot;

    try {
        // Obtener el elemento raíz del documento.
        xmlDoc = parser->getDocument();
        elementRoot = xmlDoc->getDocumentElement();

        if(!elementRoot)
            throw(std::runtime_error("Documento XML vacío."));

    }
    catch (xercesc::XMLException& e ) {
        char* message = xercesc::XMLString::transcode( e.getMessage() );
        ostringstream errBuf;
        errBuf << "Error 'parseando': " << message << flush;
        XMLString::release( &message );
        return;
    }

    XMLCh* casillaNodo = XMLString::transcode("tileset");
    XMLCh* capaNodo = XMLString::transcode("layer");

    // Procesando los nodos hijos del raíz...
    for (XMLSize_t i = 0; i < elementRoot->getChildNodes()->getLength(); ++i ) {

        DOMNode* node = elementRoot->getChildNodes()->item(i);

        if (node->getNodeType() == DOMNode::ELEMENT_NODE) {
            // Nodo <tileset>?
            if (XMLString::equals(node->getNodeName(), casillaNodo)){
                // Parsear casilla
                parsearCasilla(node);
            }else{
                // Nodo <layer>?
                if (XMLString::equals(node->getNodeName(), capaNodo)){
                    // Parsear capa
                    parsearCapa(node);
                }
            }
        }
    }
    // Liberar recursos.
    XMLString::release(&casillaNodo);
    XMLString::release(&capaNodo);

    delete parser;
}

void Escenario::crearEscenario()
{
    int indiceCasilla;
    int tipoCasilla;
    Casilla* casilla;

    for(int i=0; i<this->_ancho; i++){
        for(int z = 0; z<this->_alto; z++){

            indiceCasilla = (z*this->_ancho) + i;
            tipoCasilla = this->_indiceCasillas.at(indiceCasilla);
            _itCasillas = this->_tipoCasillas.find(tipoCasilla);

            casilla = new Casilla(_itCasillas->second, Ogre::Vector3(i, 0, z), _nodoEscenario, _sceneManager, _mPhysics);

            if(_itCasillas->second.compare("edificio01")==0){
                cout << "Casilla objetivo" << endl;
                this->_objetivo = casilla;
            }else if (_itCasillas->second.compare("start")==0){
                cout << "Casilla de salida" << endl;
                this->_salida = casilla;
            }

            this->_escenario.push_back(casilla);
        }
    }

    cout << "Escenario Grafico creado" << endl;

    // Fisica

    btTransform startTransform;
    startTransform.setIdentity();

    startTransform.setOrigin(btVector3(this->_ancho/2,0,this->_alto/2));

    _colision = new btBoxShape(btVector3(this->_ancho/2, 0.06, this->_alto/2));
    _cuerpo = _mPhysics->addRigidBody(startTransform, _colision, 0, this->_nodoEscenario);
    this->_cuerpo->setFriction(0.55);

    cout << "Escenario " << this->_nombre << " cargado" << endl;
}

void Escenario::crearEnemigos()
{
    int indiceCasilla;
    int tipoCasilla;
    Enemigo* enemigo;
    bool inicio = false;
    bool final = false;
    btVector3 posicionInicial;
    btVector3 posicionFinal;

    std::map<int, string>::iterator iterador;
    for(int z=0; z<this->_alto; z++){
        for(int i = 0; i<this->_ancho; i++){
            indiceCasilla = (z*this->_ancho) + i;
            tipoCasilla = this->_indiceEnemigos.at(indiceCasilla);

            if(tipoCasilla > 0){
                // Hay posicion
                iterador = this->_tipoCasillas.find(tipoCasilla);

                // Asignar posicion inicial y final al enemigo
                if(!inicio && !final){
                    // Esta es la posicion inicial
                    posicionInicial = btVector3(i, 0, z);
                    inicio = true;

                }else if(inicio && !final){
                    // Esta es la posicion final
                    posicionFinal = btVector3(i, 0, z);
                    final = true;

                }

                if(inicio && final){
                    // Creamos el enemigo
//                    cout << "pI: " << posicionInicial.getX() << "," << posicionInicial.getZ() << endl;
//                    cout << "pF: " << posicionFinal.getX() << "," << posicionFinal.getZ() << endl;
                    enemigo = new Enemigo(iterador->second, posicionInicial, posicionFinal, _nodoEscenario, _sceneManager, _mPhysics);
                    // Reiniciamos posiciones
                    inicio = false;
                    final = false;
                    this->_enemigos.push_back(enemigo);
                }
            }
        }
    }

    cout << "Enemigos " << this->_enemigos.size() << " creados" << endl;
}

void Escenario::parsearCasilla(DOMNode *casillaNodo)
{
    // Atributos de la casilla
    DOMNamedNodeMap* attributes = casillaNodo->getAttributes();
    DOMNode* iCasilla = attributes->getNamedItem(XMLString::transcode("firstgid"));
    DOMNode* nCasilla = attributes->getNamedItem(XMLString::transcode("name"));

    int numeroCasilla = atoi(XMLString::transcode(iCasilla->getNodeValue()));
    string nombreCasilla = XMLString::transcode(nCasilla->getNodeValue());

    //    cout << "n: " << numeroCasilla << " nombre: " << nombreCasilla << endl;

    this->_tipoCasillas.insert(std::pair<int,string>(numeroCasilla, nombreCasilla));
}

void Escenario::parsearCapa(DOMNode *capaNodo)
{

    string tipoCapa;

    // Atributos de la capa
    DOMNamedNodeMap* attributes = capaNodo->getAttributes();

    DOMNode* nombre = attributes->getNamedItem(XMLString::transcode("name"));
    DOMNode* ancho = attributes->getNamedItem(XMLString::transcode("width"));
    DOMNode* alto = attributes->getNamedItem(XMLString::transcode("height"));

    this->_ancho = atoi(XMLString::transcode(ancho->getNodeValue()));
    this->_alto = atoi(XMLString::transcode(alto->getNodeValue()));

    XMLCh* data_ch = XMLString::transcode("data");

    tipoCapa = XMLString::transcode(nombre->getNodeValue());

    if(tipoCapa.compare("escenario")==0){
        // Escenario
        for (XMLSize_t i = 0; i < capaNodo->getChildNodes()->getLength(); ++i ) {

            DOMNode* node = capaNodo->getChildNodes()->item(i);

            if (node->getNodeType() == DOMNode::ELEMENT_NODE) {
                // Nodo <data>?
                if (XMLString::equals(node->getNodeName(), data_ch)){
                    parsearMapa(node);
                }
            }
        }

    }else{
        // Enemigos
        for (XMLSize_t i = 0; i < capaNodo->getChildNodes()->getLength(); ++i ) {

            DOMNode* node = capaNodo->getChildNodes()->item(i);

            if (node->getNodeType() == DOMNode::ELEMENT_NODE) {
                // Nodo <data>?
                if (XMLString::equals(node->getNodeName(), data_ch)){
                    parsearEnemigos(node);
                }
            }
        }
    }

    XMLString::release(&data_ch);

}

void Escenario::parsearMapa(DOMNode *capaMapa)
{
    XMLCh* title_ch = XMLString::transcode("tile");

    for (XMLSize_t i = 0; i < capaMapa->getChildNodes()->getLength(); ++i ) {

        DOMNode* node = capaMapa->getChildNodes()->item(i);

        if (node->getNodeType() == DOMNode::ELEMENT_NODE) {
            // Nodo <tile>?
            if (XMLString::equals(node->getNodeName(), title_ch)){

                // Atributos de la casilla
                DOMNamedNodeMap* attributes = node->getAttributes();
                DOMNode* iCasilla = attributes->getNamedItem(XMLString::transcode("gid"));

                int casilla  = atoi(XMLString::transcode(iCasilla->getNodeValue()));
                this->_indiceCasillas.push_back(casilla);
            }
        }
    }

    XMLString::release(&title_ch);
}

void Escenario::parsearEnemigos(DOMNode *capaEnemigos)
{
    XMLCh* title_ch = XMLString::transcode("tile");

    for (XMLSize_t i = 0; i < capaEnemigos->getChildNodes()->getLength(); ++i ) {

        DOMNode* node = capaEnemigos->getChildNodes()->item(i);

        if (node->getNodeType() == DOMNode::ELEMENT_NODE) {
            // Nodo <tile>?
            if (XMLString::equals(node->getNodeName(), title_ch)){

                // Atributos de la casilla
                DOMNamedNodeMap* attributes = node->getAttributes();
                DOMNode* iCasilla = attributes->getNamedItem(XMLString::transcode("gid"));

                int casilla  = atoi(XMLString::transcode(iCasilla->getNodeValue()));
                this->_indiceEnemigos.push_back(casilla);
            }
        }
    }

    XMLString::release(&title_ch);
}


int Escenario::getAncho()
{
    return this->_ancho;
}

int Escenario::getAlto()
{
    return this->_alto;
}

void Escenario::actualizarEnemigos()
{
    for(_itEnemigos = _enemigos.begin(); _itEnemigos != _enemigos.end(); ++_itEnemigos){

        (*_itEnemigos)->actualizarPosicion();
    }
}

std::vector<Enemigo *> Escenario::getEnemigos()
{
    return this->_enemigos;
}

void Escenario::destruirEnemigo(Enemigo *enemigo)
{
    _itEnemigos = std::find(_enemigos.begin(), _enemigos.end(), enemigo);
    if (_itEnemigos != _enemigos.end())
        delete *_itEnemigos;
    _enemigos.erase(_itEnemigos);
}

Casilla *Escenario::getObjetivo()
{
    return this->_objetivo;
}

Casilla *Escenario::getStart()
{
    return this->_salida;
}

string Escenario::getAudio()
{
    return this->_audio;
}

