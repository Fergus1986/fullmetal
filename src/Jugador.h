#ifndef JUGADOR_H
#define JUGADOR_H

#define RUN_SPEED   2.5
#define TURN_SPEED  300

#include "PhysicsManager.hpp"
#include "CharacterControllerPhysics.hpp"

#include "OGRE/Ogre.h"
#include <OIS/OIS.h>

class Jugador
{
public:
    Jugador();
    Jugador(
            string nombre,
            int vidas,
            SceneManager* Manager,
            Camera* camara,
            btVector3 posicion,
            PhysicsManager* Physics);

    ~Jugador();

    Ogre::Vector3 getPosicion();
//    CharacterControllerManager* getCharacter();
    void actualizar(Real deltaTime);
    bool matar();
    int getVidas();
    void injectKeyDown(const OIS::KeyEvent & evt);
    void injectKeyUp(const OIS::KeyEvent & evt);
    CharacterControllerPhysics* getCCPhysics();
    SceneNode* getNodo();

private:

    string _nombre;
    int _vidas;

    //Ogre
    SceneManager* _sceneMgr;
    Camera* _camara;
    Entity* _entidad;
    SceneNode* _nodo;

    //Fisica
    btVector3 _posicionInicial;
    btConvexShape * _colision;
    PhysicsManager* _physics;
    CharacterControllerPhysics* mCCPhysics;
    btPairCachingGhostObject * characterGhostObject;
//    CharacterControllerManager* mCharacter;

    // Camara y direccion

    SceneNode * mCameraGoal;
    Vector3 mGoalDirection;
    Vector3 mKeyDirection;
    SceneNode * mCameraNode;
    SceneNode * mCameraPivot;


};

#endif // JUGADOR_H
