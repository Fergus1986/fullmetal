#ifndef GESTIONARTIEMPO_H
#define GESTIONARTIEMPO_H

#include <string>
#include <sstream>

using namespace std;

class GestionarTiempo
{
public:
    GestionarTiempo(int m, int s);
    ~GestionarTiempo();
    int getMinuto();
    int getSegundo();
    void setMinuto(int m);
    void setSegundo(int s);
    void setContSegundos(int s);
    int getContSegundos();
    void contarTiempo();
    string getTiempo();
    bool finTiempo();
private:
    int _minuto;
    int _segundo;
    int _contSegundos;
};

#endif // GESTIONARTIEMPO_H
