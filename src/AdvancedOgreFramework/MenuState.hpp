#ifndef MENU_STATE_HPP
#define MENU_STATE_HPP

#include "AppState.hpp"


class MenuState : public AppState
{
public:
    MenuState();

	DECLARE_APPSTATE_CLASS(MenuState)

	void enter();
	void createScene();
	void exit();
    bool pause();
    void resume();

	bool keyPressed(const OIS::KeyEvent &keyEventRef);
	bool keyReleased(const OIS::KeyEvent &keyEventRef);

	bool mouseMoved(const OIS::MouseEvent &evt);
	bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
	bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

	void buttonHit(OgreBites::Button* button);
    void yesNoDialogClosed(const Ogre::DisplayString& question, bool yesHit);

	void update(double timeSinceLastFrame);

private:
	bool m_bQuit;
    bool m_bQuestionActive;
    Ogre::OverlayManager* _overlayManager;
    Ogre::Overlay *fondo;
    Ogre::Overlay *tanque;
    Ogre::Overlay *menu;

    // Tiempo
    double _tiempo;
    int _segundos;

    // Musica
    TrackManager*				_trackMgr;
    SoundFXManager*				_soundFxMgr;
    TrackPtr 					_musicTrack;
};


#endif
