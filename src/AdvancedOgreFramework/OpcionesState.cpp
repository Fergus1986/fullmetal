#include "OpcionesState.h"

OpcionesState::OpcionesState()
{
    m_bQuit = false;

}


void OpcionesState::enter()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Entrando en Opciones...");

    m_pSceneMgr = OgreFramework::getSingletonPtr()->m_pRoot->createSceneManager(ST_GENERIC, "OpcionesSceneMgr");
    m_pSceneMgr->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));

    m_pCamera = m_pSceneMgr->createCamera("OpcionesCam");
    m_pCamera->setPosition(Vector3(0, 25, -50));
    m_pCamera->lookAt(Vector3(0, 0, 0));
    m_pCamera->setNearClipDistance(1);

    m_pCamera->setAspectRatio(Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualWidth()) /
                              Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualHeight()));

    OgreFramework::getSingletonPtr()->m_pViewport->setCamera(m_pCamera);

    OgreFramework::getSingletonPtr()->m_pTrayMgr->destroyAllWidgets();

    OgreFramework::getSingletonPtr()->m_pTrayMgr->showCursor();

    this->_overlayManager = Ogre::OverlayManager::getSingletonPtr();
    fondo = _overlayManager->getByName("Fondo");
    _opcionesOverlay = _overlayManager->getByName("OpcionesOverlay");

    fondo->show();
    _opcionesOverlay->show();

    cout << "Overlays Opciones creados" << endl;

    _niveles.clear();

    leerOpciones();
    OgreFramework::getSingletonPtr()->m_pTrayMgr->createCheckBox(OgreBites::TL_CENTER,"Sonido","Desactivar sonido",250);
    OgreFramework::getSingletonPtr()->m_pTrayMgr->createLongSelectMenu(OgreBites::TL_CENTER, "Nivel","Elegir nivel",250, _niveles.size(),_niveles);

    createScene();
    m_bQuit = false;


}

void OpcionesState::createScene()
{

}

void OpcionesState::exit(){

    guardarOpciones();

    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Salir de Opciones...");

    m_pSceneMgr->destroyCamera(m_pCamera);
    if(m_pSceneMgr){
        OgreFramework::getSingletonPtr()->m_pRoot->destroySceneManager(m_pSceneMgr);
    }

    OgreFramework::getSingletonPtr()->m_pTrayMgr->clearAllTrays();
    OgreFramework::getSingletonPtr()->m_pTrayMgr->destroyAllWidgets();
    OgreFramework::getSingletonPtr()->m_pTrayMgr->setListener(0);

    fondo->hide();
    _opcionesOverlay->hide();

}

bool OpcionesState::keyPressed(const OIS::KeyEvent &keyEventRef){

    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_ESCAPE))
    {
        cout << "ESC presionado" << endl;
        this->m_bQuit=true;
    }

    OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);
    return true;
}

bool OpcionesState::keyReleased(const OIS::KeyEvent &keyEventRef)
{

}

bool OpcionesState::mouseMoved(const OIS::MouseEvent &evt)
{
    if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseMove(evt)) return true;
    return true;
}

bool OpcionesState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseDown(evt, id)) return true;
    return true;
}

bool OpcionesState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseUp(evt, id)) return true;
    return true;
}

void OpcionesState::buttonHit(OgreBites::Button *button)
{


}


void OpcionesState::update(double timeSinceLastFrame)
{
    if(this->m_bQuit){
        //        popAppState();
        this->popAllAndPushAppState(findByName("Menu"));

    }
}

void OpcionesState::leerOpciones()
{
    std::stringstream texto;

    _opciones = iniparser_load("media/opciones.ini");

    if(_opciones == NULL){
        cerr << "No se puede cargar las opciones" << endl;

    }else{

        int nNiveles = iniparser_getint(_opciones, "niveles:numero", -1);
        int sonido = iniparser_getint(_opciones, "sonido:activado", -1);
        int volumen = iniparser_getint(_opciones, "sonido:volumen", -1);


        for (int i=1; i<= nNiveles; i++){
            std::stringstream clave;
            clave << "niveles:nivel" << i;
            _niveles.push_back(iniparser_getstring(_opciones, clave.str().c_str(), NULL));
            texto << "Nivel" << i << "-";
            texto << iniparser_getstring(_opciones, clave.str().c_str(), NULL) << "\n";
        }

        cout << "sonido: " << sonido << endl;
        cout << "volumen: " << volumen << endl;
        cout << "opciones:\n" << texto.str() << endl;
    }
}

void OpcionesState::guardarOpciones()
{
    cout << "Guardando opciones" << endl;

    FILE* archivo;
    archivo = fopen("media/opciones.ini", "w");

    iniparser_dump_ini(_opciones, archivo);

    fclose (archivo);

    iniparser_freedict(_opciones);
}

void OpcionesState::checkBoxToggled(OgreBites::CheckBox* box)
{

    if(box->isChecked()){
        cout << "sonido desactivado" << endl;
        iniparser_set(_opciones, "sonido:activado", "0");

    }
    else{
        cout << "sonido activado" << endl;
        iniparser_set(_opciones, "sonido:activado", "1");

    }
}

void OpcionesState::itemSelected(OgreBites::SelectMenu *menu)
{
    string nivel =  menu->getSelectedItem();
    cout << "n: " << nivel.c_str() << endl;
    iniparser_set(_opciones, "nivel actual:nivel", nivel.c_str());

}
