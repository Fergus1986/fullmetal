#include "GameState.hpp"

using namespace Ogre;


GameState::GameState()
{

    m_bQuit = false;
    _trackMgr =  OgreFramework::getSingletonPtr()->m_pAudioMgr;
    _soundFxMgr =  OgreFramework::getSingletonPtr()->m_pSoundMgr;

}


void GameState::enter()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Entrando en Jugar...");

    m_pSceneMgr = OgreFramework::getSingletonPtr()->m_pRoot->createSceneManager(ST_GENERIC, "GameSceneMgr");
    m_pSceneMgr->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));
    m_pSceneMgr->createLight("Light")->setPosition(75,75,75);
    m_pSceneMgr->setShadowTechnique(SHADOWTYPE_STENCIL_MODULATIVE);

    m_pCamera = m_pSceneMgr->createCamera("GameCamera");
    m_pCamera->setPosition(Vector3(5, 4, 13));
    m_pCamera->lookAt(Vector3(5, -5, 0));
    m_pCamera->setNearClipDistance(0.05);

    m_pCamera->setAspectRatio(Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualWidth()) /
                              Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualHeight()));

    OgreFramework::getSingletonPtr()->m_pViewport->setCamera(m_pCamera);
    m_pSceneMgr->setSkyDome(true, "Cielo",1,1, 50000);

    m_bQuit = false;
    _victoria = false;

    // Fisica
    mPhysics = new PhysicsManager();
    mPhysics->setRootSceneNode(m_pSceneMgr->getRootSceneNode());
    mDebugDrawer = new DebugDrawer(m_pSceneMgr->getRootSceneNode(), mPhysics->getDynamicsWorld());
    mPhysics->getDynamicsWorld()->setDebugDrawer(mDebugDrawer);
    mEnabledPhysicsDebugDraw = false;
    mDebugDrawer->setDebugMode(mEnabledPhysicsDebugDraw);

    // Explosion
    _explosion = false;
    _tiempoExplosion = 0.0;
    _sonidoExplosion = this->_soundFxMgr->load("explosion.wav");

    _nodoExplosion = m_pSceneMgr->createSceneNode();
    _entidadExplosion = m_pSceneMgr->createEntity("explosion.mesh");
    _nodoExplosion->attachObject(_entidadExplosion);
    m_pSceneMgr->getRootSceneNode()->addChild(_nodoExplosion);
    _nodoExplosion->setVisible(false);

    // Overlays
    this->_overlayManager = Ogre::OverlayManager::getSingletonPtr();

    this->_tiempoOverlay = this->_overlayManager->getByName("Contador");
    this->_OETiempo = this->_overlayManager->getOverlayElement("time");

    this->_vida1Overlay = this->_overlayManager->getByName("Vida");
    this->_vida2Overlay = this->_overlayManager->getByName("Vida2");
    this->_vida3Overlay = this->_overlayManager->getByName("Vida3");

    // Niveles

    _niveles.push_back("media/nivel1.tmx");
    _niveles.push_back("media/nivel2.tmx");
    _niveles.push_back("media/nivel3.tmx");

    createScene();
}

bool GameState::pause()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Pausar Jugar...");
//    this->_musica->pause();
    ocultarGUI();

    return true;
}


void GameState::resume()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Volver a Jugar...");
    buildGUI();
    OgreFramework::getSingletonPtr()->m_pViewport->setCamera(m_pCamera);
    m_bQuit = false;

    if(this->_niveles.size()==0){
        // El jugador terminó el último nivel
        this->_musica->pause();
        this->_musica->unload();
        this->popAllAndPushAppState(findByName("Menu"));
        return;

    }else if(this->_victoria){
        this->createScene();
        this->_victoria = false;
    }else{
//        this->_musica->play(-1);
    }
}


void GameState::exit()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Salir de Jugar...");

    m_pSceneMgr->destroyCamera(m_pCamera);
    if(m_pSceneMgr){
        OgreFramework::getSingletonPtr()->m_pRoot->destroySceneManager(m_pSceneMgr);
    }

    OgreFramework::getSingletonPtr()->m_pTrayMgr->clearAllTrays();
    OgreFramework::getSingletonPtr()->m_pTrayMgr->destroyAllWidgets();
    OgreFramework::getSingletonPtr()->m_pTrayMgr->setListener(0);

    this->_musica->stop();
    this->_musica->unload();

    ocultarGUI();

    delete mPhysics;

    cout << "Destruido estado Jugar" << endl;
}


void GameState::createScene()
{
    // Leer opciones

    _nivelActual = _niveles.front();

    cout << "Nivel actual: " << _nivelActual << endl;

    _musicaNivel = "14-Cascanueces- Vals de las flores.mp3";
    this->_escenario = new Escenario(_nivelActual, m_pSceneMgr, mPhysics);
    btVector3 posicion = btVector3(_escenario->getStart()->getPosicion().x, 5, _escenario->getStart()->getPosicion().z);
    this->_jugador = new Jugador("personaje", 3, m_pSceneMgr, m_pCamera, posicion, mPhysics);

    this->_gtime = new GestionarTiempo(1,10);
    this->_contTimeSLF = 0.0;

    // Musica
    _musica = this->_trackMgr->load(_musicaNivel);
    this->_musica->fadeIn(128, -1, 1000, 0);
    cout << "Musica cargada" << endl;

    // Fisica
    crearCallBack();

    _tiempo = 0.0;
    _segundos = 0;
    m_pCamera->setPosition(Vector3(_escenario->getAncho()/2, _escenario->getAlto()/4, _escenario->getAlto()));

    buildGUI();
    cout << "Escena creada" << endl;
}


bool GameState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);

    this->_jugador->injectKeyDown(keyEventRef);

    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_ESCAPE))
    {
        pushAppState(findByName("Pausa"));
    }

    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_TAB)){
        cout << "TAB presionado" << endl;
        mEnabledPhysicsDebugDraw = !mEnabledPhysicsDebugDraw;
        mDebugDrawer->setDebugMode(mEnabledPhysicsDebugDraw);
    }


    return true;
}

bool GameState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
    this->_jugador->injectKeyUp(keyEventRef);

    OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);
    return true;
}

bool GameState::mouseMoved(const OIS::MouseEvent &evt)
{
    if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseMove(evt)) return true;

    return true;
}


bool GameState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseDown(evt, id)) return true;

    return true;
}


bool GameState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseUp(evt, id)) return true;

    return true;
}



void GameState::moveCamera()
{
    // Posicion del Jugador
    _camaraX = this->_jugador->getPosicion().x;
    _camaraZ = this->_jugador->getPosicion().z;

    // Restringimos el movimiento de la camara
    if (_camaraX > _escenario->getAncho()/2){
        _camaraX = min(Ogre::Real(18.0), _camaraX);
    }else{
        _camaraX = max(Ogre::Real(10.0), _camaraX);
    }

    _camaraZ = min(Ogre::Real(10.0), _camaraZ);

    // Asignamos la posición a la camara
    _centroCamara = Ogre::Vector3(_camaraX, this->_jugador->getPosicion().y, _camaraZ);


    m_pCamera->lookAt(_centroCamara);
}


void GameState::getInput()
{
}

void GameState::ocultarGUI()
{
    this->_vida1Overlay->hide();
    this->_vida2Overlay->hide();
    this->_vida3Overlay->hide();
    this->_tiempoOverlay->hide();
    this->_OETiempo->hide();
}


void GameState::update(double timeSinceLastFrame)
{
    m_FrameEvent.timeSinceLastFrame = timeSinceLastFrame;
    OgreFramework::getSingletonPtr()->m_pTrayMgr->frameRenderingQueued(m_FrameEvent);

    // Comprobar condiciones de salida del Juego
    if(_gtime->finTiempo()){
        // Se ha acabado el tiempo
        crearExplosion();
        if(this->_jugador->matar()){
            // Fin del Juego
            m_bQuit=true;
        }else{
            // Al jugador le hemos restado una vida por fin de tiempo

            dibujarVida();
            //            this->_nodoExplosion->setVisible(false);
            //            this->_explosion = true;
            //            _tiempoExplosion=0.0;
            // Reseteo del tiempo
            this->_gtime = new GestionarTiempo(1,10);
            this->_contTimeSLF = 0.0;
        }
    }

    if(m_bQuit)
    {
        // El Jugador ha muerto
        this->popAllAndPushAppState(findByName("GameOver"));
        return;
    }

    if(_victoria){
        // El Jugar ha ganado
        delete this->_escenario;
        delete this->_jugador;
        _niveles.erase(_niveles.begin());

        cout << "Victoria" << endl;
        pushAppState(findByName("Enhorabuena"));
        return;
    }

    // Gestion de Tiempo
    if(_segundos >= 2){
        this->_escenario->actualizarEnemigos();
    }

    this->_contTimeSLF += timeSinceLastFrame;
    if(_contTimeSLF>=GAMMA){
        _gtime->contarTiempo();
        _segundos += 1;
        _contTimeSLF=0;
    }

    this->_OETiempo->setCaption(this->_gtime->getTiempo());

    // Actualizar el jugador
    this->_jugador->actualizar(timeSinceLastFrame);

    // Actualizar la Fisica
    mPhysics->getDynamicsWorld()->stepSimulation(timeSinceLastFrame);
    mDebugDrawer->step();

    // Comprobar colisiones
    this->mPhysics->getDynamicsWorld()->contactTest(this->_jugador->getCCPhysics()->getGhostObject(), *callback);

    // Actualizar explosion
    if(this->_explosion){
        _tiempoExplosion += timeSinceLastFrame;
        cout << "Explosion" << endl;

        if(_tiempoExplosion >= 1.5){
            // Hacer no visible la explosion
            this->_nodoExplosion->setVisible(false);
            _tiempoExplosion = 0.0;
            _explosion = false;
        }
    }

    // Actualizar camara
    moveCamera();

}

void GameState::crearCallBack()
{
    // Crear callback
    callback = new ContactSensorCallback(this->_escenario->getEnemigos(),_escenario->getObjetivo()->getCuerpo(),this->_jugador,this);
}

void GameState::colisionEnemigo(Enemigo *e)
{
    crearExplosion();
    e->getNodo()->setVisible(false);
    this->_escenario->destruirEnemigo(e);

    // Actualizar lista de enemigos para el CallBack
    crearCallBack();

    if(this->_jugador->matar()){
        // Fin de Juego
        cout << "Jugador muerto" << endl;
        m_bQuit = true;

    }else{
        dibujarVida();

    }

    cout << "Colision terminada" << endl;

}

void GameState::dibujarVida()
{
    this->_tiempoOverlay->show();
    this->_OETiempo->show();

    switch(this->_jugador->getVidas()){

    case 1:
        _vida1Overlay->show();
        _vida2Overlay->hide();
        _vida3Overlay->hide();
        break;

    case 2:
        _vida1Overlay->show();
        _vida2Overlay->show();
        _vida3Overlay->hide();
        break;

    case 3:
        _vida1Overlay->show();
        _vida2Overlay->show();
        _vida3Overlay->show();
        break;

    case 0:
        _vida1Overlay->hide();
        _vida2Overlay->hide();
        _vida3Overlay->hide();
        break;
    }
}

void GameState::crearExplosion()
{
    // Explosion
    this->_explosion = true;
    this->_nodoExplosion->setPosition(this->_jugador->getPosicion());
    this->_nodoExplosion->setVisible(true);
    this->_sonidoExplosion->play(0);
}


void GameState::buildGUI()
{
    //    OgreFramework::getSingletonPtr()->m_pTrayMgr->showFrameStats(OgreBites::TL_BOTTOMLEFT);
    //    OgreFramework::getSingletonPtr()->m_pTrayMgr->showCursor();

    //    Ogre::StringVector displayModes;
    //    displayModes.push_back("Solid mode");
    //    displayModes.push_back("Wireframe mode");
    //    OgreFramework::getSingletonPtr()->m_pTrayMgr->createLongSelectMenu(OgreBites::TL_TOPRIGHT, "DisplayModeSelMenu", "Display Mode", 200, 3, displayModes);

    dibujarVida();

    cout << "GUI creada" << endl;
}


void GameState::itemSelected(OgreBites::SelectMenu* menu)
{
    switch(menu->getSelectionIndex())
    {
    case 0:
        m_pCamera->setPolygonMode(Ogre::PM_SOLID);break;
    case 1:
        m_pCamera->setPolygonMode(Ogre::PM_WIREFRAME);break;
    }
}

