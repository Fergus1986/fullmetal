#ifndef CASILLA_H
#define CASILLA_H

#include <string>

#include <Ogre.h>

#include "PhysicsManager.hpp"

using namespace Ogre;
using namespace std;

class Casilla
{
public:
    Casilla();
    Casilla(string nombre, Vector3 posicion, SceneNode *escenario, SceneManager *sceneManager, PhysicsManager *Physics);
    ~Casilla();

    void setPosicion(Ogre::Vector3 posicion);
    void setRotacion(Ogre::Degree grados);
    Ogre::Vector3 getPosicion();
    btRigidBody* getCuerpo();


private:

    PhysicsManager *_physics;
    SceneManager* _sceneMgr;
    Entity* _entidad;
    SceneNode* _nodo;
    Ogre::Vector3 _posicion;
    btCollisionShape* _colision;
    btRigidBody* _cuerpo;
};

#endif // CASILLA_H
