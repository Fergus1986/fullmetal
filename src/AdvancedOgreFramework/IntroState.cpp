#include "IntroState.h"

IntroState::IntroState()
{
    m_bQuit = false;

    _trackMgr =  OgreFramework::getSingletonPtr()->m_pAudioMgr;
    _soundFxMgr =  OgreFramework::getSingletonPtr()->m_pSoundMgr;

}

void IntroState::enter()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Entrando en Intro...");
    OgreFramework::getSingletonPtr()->m_pTrayMgr->showCursor();

    m_pSceneMgr = OgreFramework::getSingletonPtr()->m_pRoot->createSceneManager(ST_GENERIC, "IntroSceneMgr");
    m_pSceneMgr->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));
    m_pSceneMgr->setShadowTechnique(SHADOWTYPE_STENCIL_MODULATIVE);

    m_pCamera = m_pSceneMgr->createCamera("IntroCam");
    m_pCamera->setPosition(Vector3(7, 5, 10));
    m_pCamera->lookAt(Vector3(7, 0, 5.5));
    m_pCamera->setNearClipDistance(1);

    m_pCamera->setAspectRatio(Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualWidth()) /
                              Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualHeight()));

    OgreFramework::getSingletonPtr()->m_pViewport->setCamera(m_pCamera);

    cout << "Ventana y Camara Intro creadas" << endl;

    this->_overlayManager = Ogre::OverlayManager::getSingletonPtr();
    _intro = _overlayManager->getByName("Intro");
    _intro->show();

    cout << "Overlay Intro creado" << endl;

    // Fisica
    mPhysics = new PhysicsManager();
    mPhysics->setRootSceneNode(m_pSceneMgr->getRootSceneNode());
    mDebugDrawer = new DebugDrawer(m_pSceneMgr->getRootSceneNode(), mPhysics->getDynamicsWorld());
    mPhysics->getDynamicsWorld()->setDebugDrawer(mDebugDrawer);
    mEnabledPhysicsDebugDraw = false;
    mDebugDrawer->setDebugMode(mEnabledPhysicsDebugDraw);

    cout << "Fisica Intro creada" << endl;

    // Sonido
    _trackMgr =  OgreFramework::getSingletonPtr()->m_pAudioMgr;
    _soundFxMgr =  OgreFramework::getSingletonPtr()->m_pSoundMgr;

    cout << "Sonido Intro creado" << endl;

    createScene();
    m_bQuit = false;


}

void IntroState::createScene()
{
    cout << "Creando Escena Intro" << endl;

    m_pSceneMgr->createLight("Light")->setPosition(75,75,75);

    this->_escenario = new Escenario("media/nivel0.tmx", this->m_pSceneMgr, this->mPhysics);

    _tiempo = 0.0;
    _segundos = 0;

    _musicTrack = this->_trackMgr->load("01-El lago de los cisnes- Escena.mp3");

    cout << "Musica cargada" << endl;

    this->_musicTrack->fadeIn(128, -1, 1000, 54);

    cout << "Escena Intro creada" << endl;

}

void IntroState::exit()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Salir de Intro...");

    m_pSceneMgr->destroyCamera(m_pCamera);
    if(m_pSceneMgr){
        OgreFramework::getSingletonPtr()->m_pRoot->destroySceneManager(m_pSceneMgr);
    }
    _intro->hide();
    this->_musicTrack->stop();
    this->_musicTrack->unload();

    delete mPhysics;
}

bool IntroState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_ESCAPE))
    {
        cout << "ESC presionado" << endl;
        this->m_bQuit=true;
    }

    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_RETURN))
    {
        cout << "Intro presionado" << endl;
        changeAppState(findByName("Menu"));
    }

    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_TAB)){
        cout << "TAB presionado" << endl;
        mEnabledPhysicsDebugDraw = !mEnabledPhysicsDebugDraw;
        mDebugDrawer->setDebugMode(mEnabledPhysicsDebugDraw);
    }

    OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);

    return true;

}

bool IntroState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
}

bool IntroState::mouseMoved(const OIS::MouseEvent &evt)
{
    if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseMove(evt)) return true;
    return true;
}

bool IntroState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{

}

bool IntroState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
}

void IntroState::buttonHit(OgreBites::Button *button)
{
}

void IntroState::update(double timeSinceLastFrame)
{

    m_FrameEvent.timeSinceLastFrame = timeSinceLastFrame;
    OgreFramework::getSingletonPtr()->m_pTrayMgr->frameRenderingQueued(m_FrameEvent);

    if(this->m_bQuit){
        shutdown();
    }

    // Update physics
    mPhysics->getDynamicsWorld()->stepSimulation(timeSinceLastFrame);
    mDebugDrawer->step();

    _tiempo += timeSinceLastFrame;

    if(_tiempo >= 1.0){
        _segundos += 1;
        _tiempo = 0;
        cout << "t:" << _segundos << endl;
    }

    if(_segundos >= 1){
        this->_escenario->actualizarEnemigos();
    }

    if(_segundos >= TIEMPO_INTRO){
        this->_musicTrack->stop();
        changeAppState(findByName("Menu"));
    }
}
