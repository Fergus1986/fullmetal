#include "Jugador.h"

Jugador::Jugador()
{
}

Jugador::Jugador(
        string nombre,
        int vidas,
        SceneManager* Manager,
        Camera* camara,
        btVector3 posicion,
        PhysicsManager* Physics)
{

    this->_sceneMgr = Manager;
    this->_physics = Physics;
    this->_camara = camara;
    this->_posicionInicial = posicion;

    this->_nombre = nombre;
    this->_vidas = vidas;

    // Nodo y entidad
    this->_nodo = _sceneMgr->createSceneNode();
    this->_entidad = _sceneMgr->createEntity(nombre.append(".mesh"));

    _nodo->attachObject(_entidad);
    this->_nodo->translate(_posicionInicial.x(),_posicionInicial.y(), _posicionInicial.z());
    _sceneMgr->getRootSceneNode()->addChild(this->_nodo);

    // Fisica
    btTransform startTransform;
    startTransform.setIdentity();
    startTransform.setOrigin(_posicionInicial);

    characterGhostObject = new btPairCachingGhostObject();
    characterGhostObject->setWorldTransform(startTransform);

    this->_colision = new btCapsuleShape(0.4,0.3);

    _physics->addCollisionShape(_colision);
    characterGhostObject->setCollisionShape(_colision);
    characterGhostObject->setCollisionFlags(btCollisionObject::CF_CHARACTER_OBJECT);

    btScalar stepHeight = 0.2f;

    mCCPhysics = new CharacterControllerPhysics(characterGhostObject, _colision, stepHeight, _physics->getCollisionWorld(), 1);


    _physics->getBroadphase()->getOverlappingPairCache()->setInternalGhostPairCallback(new btGhostPairCallback());

    _physics->getDynamicsWorld()->addCollisionObject(characterGhostObject, btBroadphaseProxy::CharacterFilter, btBroadphaseProxy::StaticFilter | btBroadphaseProxy::DefaultFilter);
    _physics->getDynamicsWorld()->addAction(mCCPhysics);


    // Camara
    mCameraPivot = this->_sceneMgr->getRootSceneNode()->createChildSceneNode();

    mCameraGoal = mCameraPivot->createChildSceneNode(Vector3(0, 0, 0));

    mCameraNode = _sceneMgr->getRootSceneNode()->createChildSceneNode();
    mCameraNode->setPosition(mCameraPivot->getPosition() + mCameraGoal->getPosition());

    mCameraPivot->setFixedYawAxis(true);
    mCameraGoal->setFixedYawAxis(true);
    mCameraNode->setFixedYawAxis(true);

//    cam->setNearClipDistance(0.1);
//    mCameraNode->attachObject(cam);
//    mCameraNode->setAutoTracking(true, mCameraPivot);


}

Jugador::~Jugador()
{
    this->_sceneMgr->destroyEntity(this->_entidad);
    this->_physics->getDynamicsWorld()->removeCollisionObject(characterGhostObject);
    this->_physics->getDynamicsWorld()->removeCharacter(this->mCCPhysics);
//    delete this->mCCPhysics;
}

Vector3 Jugador::getPosicion()
{
    return this->_nodo->getPosition();
}


void Jugador::actualizar(Ogre::Real deltaTime)
{
    Ogre::Real direction = RUN_SPEED * deltaTime;
    Vector3 playerPos = this->_nodo->getPosition();
    btVector3 pos = mCCPhysics->getPosition();

    Vector3 position(pos.x(), pos.y(), pos.z());

    if (position != playerPos)
    {
        this->_nodo->translate((position - playerPos) * direction);
    }

    mGoalDirection = Vector3::ZERO;   // we will calculate this

    if (mKeyDirection != Vector3::ZERO)
    {
        // calculate actuall goal direction in world based on player's key directions
        mGoalDirection += mKeyDirection.z * mCameraNode->getOrientation().zAxis();
        mGoalDirection += mKeyDirection.x * mCameraNode->getOrientation().xAxis();
        mGoalDirection.y = 0;
        mGoalDirection.normalise();

        Quaternion toGoal = this->_nodo->getOrientation().zAxis().getRotationTo(mGoalDirection);

        // calculate how much the character has to turn to face goal direction
        Real yawToGoal = toGoal.getYaw().valueDegrees();
        // this is how much the character CAN turn this frame
        Real yawAtSpeed = yawToGoal / Math::Abs(yawToGoal) * deltaTime * TURN_SPEED;

        // turn as much as we can, but not more than we need to
        if (yawToGoal < 0) yawToGoal = std::min<Real>(0, std::max<Real>(yawToGoal, yawAtSpeed));
        else if (yawToGoal > 0) yawToGoal = std::max<Real>(0, std::min<Real>(yawToGoal, yawAtSpeed));

        this->_nodo->yaw(Degree(yawToGoal));

        mCCPhysics->setWalkDirection(mGoalDirection.x * direction, mGoalDirection.y * direction, mGoalDirection.z * direction);
//        mCCPlayer->setIsMoving(true);
    }
    else
    {
        mCCPhysics->setWalkDirection(0, 0, 0);
//        mCCPlayer->setIsMoving(false);
    }

//	updateCamera(deltaTime);
//    mCCPlayer->addTime(deltaTime);

    cout << "Jugador actualizado" << endl;
}

bool Jugador::matar()
{
    this->_vidas -=1;

    this->mCCPhysics->setPosicion(this->_posicionInicial);


    if (_vidas<=0){
        return true;
    }else{
        return false;
    }
}

int Jugador::getVidas()
{
    return this->_vidas;
}

void Jugador::injectKeyDown(const OIS::KeyEvent &evt)
{
    if(evt.key == OIS::KC_UP)
        mKeyDirection.z = -1;

    if(evt.key == OIS::KC_DOWN)
        mKeyDirection.z = 1;

    if(evt.key == OIS::KC_LEFT)
        mKeyDirection.x = -1;

    if(evt.key == OIS::KC_RIGHT)
        mKeyDirection.x = 1;
}

void Jugador::injectKeyUp(const OIS::KeyEvent &evt)
{
    if(evt.key == OIS::KC_UP || evt.key == OIS::KC_DOWN)
        mKeyDirection.z = 0;

    if(evt.key == OIS::KC_LEFT || evt.key == OIS::KC_RIGHT)
        mKeyDirection.x = 0;
}

CharacterControllerPhysics *Jugador::getCCPhysics()
{
    return this->mCCPhysics;
}

SceneNode *Jugador::getNodo()
{
    return this->_nodo;
}
