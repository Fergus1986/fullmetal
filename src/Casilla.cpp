#include "Casilla.h"

#include <iostream>

Casilla::Casilla()
{
}

Casilla::Casilla(string nombre,
                 Ogre::Vector3 posicion,
                 SceneNode* escenario,
                 SceneManager *sceneManager,
                 PhysicsManager *Physics
                 )
{

    this->_sceneMgr = sceneManager;
    this->_physics = Physics;

    // Nodo y entidad
    this->_nodo = _sceneMgr->createSceneNode();
    this->_entidad = _sceneMgr->createEntity(nombre.append(".mesh"));

    _nodo->attachObject(_entidad);
    escenario->addChild(this->_nodo);

    // Posicion
    this->_posicion = posicion;
    this->setPosicion(this->_posicion);

    // Fisica
    _cuerpo = NULL;
    btTransform startTransform;
    startTransform.setIdentity();
    startTransform.setOrigin(btVector3(posicion.x, posicion.y, posicion.z));

    if(nombre.compare("agua.mesh")==0){
        _colision = new btBoxShape(btVector3(0.4,1,0.4));
        this->_cuerpo = _physics->addRigidBody(startTransform, _colision, 0, _nodo);

    }else if(nombre.compare("pared.mesh")==0){
        _colision = new btBoxShape(btVector3(0.5,1,0.5));
        this->_nodo->setVisible(false);
        this->_cuerpo = _physics->addRigidBody(startTransform, _colision, 0, _nodo);

    }else if(nombre.compare("arbol.mesh")==0){
        _colision = new btBoxShape(btVector3(0.35,1,0.35));
        this->_cuerpo = _physics->addRigidBody(startTransform, _colision, 0, _nodo);

    }else if(nombre.compare("edificio01.mesh")==0){
        _colision = new btBoxShape(btVector3(0.4,1,0.4));
        this->_cuerpo = _physics->addRigidBody(startTransform, _colision, 0, _nodo);

    }else{

    }
}


Casilla::~Casilla()
{
    _sceneMgr->destroyEntity(this->_entidad);
    _sceneMgr->getRootSceneNode()->removeChild(this->_nodo);

    if(_cuerpo!= NULL){
        _physics->getDynamicsWorld()->removeRigidBody(_cuerpo);
    }
}

void Casilla::setPosicion(Vector3 posicion)
{
    _nodo->translate(posicion);
}

void Casilla::setRotacion(Ogre::Degree grados)
{
    _nodo->yaw(grados, Ogre::Node::TS_LOCAL);
}

Vector3 Casilla::getPosicion()
{
    return this->_posicion;
}

btRigidBody *Casilla::getCuerpo()
{
    return this->_cuerpo;
}
