#ifndef GAMEOVERSTATE_H
#define GAMEOVERSTATE_H

#include <iostream>

#include "AppState.hpp"

#include "OGRE/Ogre.h"

using namespace Ogre;
using namespace std;

class GameOverState: public AppState
{
public:
    GameOverState();
    DECLARE_APPSTATE_CLASS(GameOverState)

    void enter();
    void createScene();
    void exit();

    bool keyPressed(const OIS::KeyEvent &keyEventRef);
    bool keyReleased(const OIS::KeyEvent &keyEventRef);

    bool mouseMoved(const OIS::MouseEvent &evt);
    bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
    bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

    void buttonHit(OgreBites::Button* button);

    void update(double timeSinceLastFrame);

private:
    bool m_bQuit;
    // Overlays
    Ogre::OverlayManager* _overlayManager;
    Ogre::Overlay *fondo;
    Ogre::Overlay *_gameOverOverlay;
    Ogre::Overlay *tanque;
};

#endif // GAMEOVERSTATE_H
