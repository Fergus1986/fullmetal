#include "MenuState.hpp"

using namespace Ogre;

MenuState::MenuState()
{
    m_bQuit         = false;
    m_FrameEvent    = Ogre::FrameEvent();
    m_bQuestionActive   = false;

    // Sonido
    _trackMgr =  OgreFramework::getSingletonPtr()->m_pAudioMgr;
    _soundFxMgr =  OgreFramework::getSingletonPtr()->m_pSoundMgr;
}


void MenuState::enter()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Entrando en Menu...");

    m_pSceneMgr = OgreFramework::getSingletonPtr()->m_pRoot->createSceneManager(ST_GENERIC, "MenuSceneMgr");
    m_pSceneMgr->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));

    m_pCamera = m_pSceneMgr->createCamera("MenuCam");
    m_pCamera->setPosition(Vector3(0, 25, -50));
    m_pCamera->lookAt(Vector3(0, 0, 0));
    m_pCamera->setNearClipDistance(1);

    m_pCamera->setAspectRatio(Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualWidth()) /
                              Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualHeight()));

    OgreFramework::getSingletonPtr()->m_pViewport->setCamera(m_pCamera);

    //    OgreFramework::getSingletonPtr()->m_pTrayMgr->destroyAllWidgets();

    OgreFramework::getSingletonPtr()->m_pTrayMgr->showCursor();
    OgreFramework::getSingletonPtr()->m_pTrayMgr->createButton(OgreBites::TL_CENTER, "Jugar", "Jugar", 500);
    OgreFramework::getSingletonPtr()->m_pTrayMgr->createButton(OgreBites::TL_CENTER, "Opciones", "Opciones", 500);
    OgreFramework::getSingletonPtr()->m_pTrayMgr->createButton(OgreBites::TL_CENTER, "Tutorial", "Tutorial", 500);
    OgreFramework::getSingletonPtr()->m_pTrayMgr->createButton(OgreBites::TL_CENTER, "Logros", "Logros", 500);
    OgreFramework::getSingletonPtr()->m_pTrayMgr->createButton(OgreBites::TL_CENTER, "Acerca", "Acerca", 500);
    OgreFramework::getSingletonPtr()->m_pTrayMgr->createButton(OgreBites::TL_CENTER, "Salir", "Salir", 500);

    this->_overlayManager = Ogre::OverlayManager::getSingletonPtr();
    fondo = _overlayManager->getByName("Fondo");
    tanque = _overlayManager->getByName("Tanque");
    menu = _overlayManager->getByName("Menu");

    fondo->show();
    tanque->show();
    menu->show();

    cout << "Overlays Menu creados" << endl;

    createScene();
}


void MenuState::createScene()
{
    _tiempo = 0.0;
    _segundos = 0;

    _musicTrack = this->_trackMgr->load("08-Cascanueces- Marcha.mp3");
    cout << "Musica menu cargada" << endl;

    this->_musicTrack->fadeIn(128, -1, 1000, 1);

    cout << "Escena Menu creada" << endl;
}


void MenuState::exit()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Saliendo del Menu Estado...");

    m_pSceneMgr->destroyCamera(m_pCamera);
    if(m_pSceneMgr)
        OgreFramework::getSingletonPtr()->m_pRoot->destroySceneManager(m_pSceneMgr);

    OgreFramework::getSingletonPtr()->m_pTrayMgr->clearAllTrays();
    OgreFramework::getSingletonPtr()->m_pTrayMgr->destroyAllWidgets();
    OgreFramework::getSingletonPtr()->m_pTrayMgr->setListener(0);

    fondo->hide();
    tanque->hide();
    menu->hide();

    this->_musicTrack->stop();
    this->_musicTrack->unload();

}

bool MenuState::pause()
{
    OgreFramework::getSingletonPtr()->m_pTrayMgr->hideTrays();

    fondo->hide();
    tanque->hide();
    menu->hide();

    return true;

}

void MenuState::resume()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Volver a Menu...");
    OgreFramework::getSingletonPtr()->m_pViewport->setCamera(m_pCamera);
    m_bQuit = false;

    OgreFramework::getSingletonPtr()->m_pTrayMgr->showTrays();

    fondo->show();
    tanque->show();
    menu->show();
}


bool MenuState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_ESCAPE) && !m_bQuestionActive)
    {
        OgreFramework::getSingletonPtr()->m_pTrayMgr->showYesNoDialog("Salir", "¿Desea Salir?");
        m_bQuestionActive = true;
    }

    OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);
    return true;
}


bool MenuState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
    OgreFramework::getSingletonPtr()->keyReleased(keyEventRef);
    return true;
}


bool MenuState::mouseMoved(const OIS::MouseEvent &evt)
{
    if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseMove(evt)) return true;
    return true;
}


bool MenuState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseDown(evt, id)) return true;
    return true;
}


bool MenuState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseUp(evt, id)) return true;
    return true;
}


void MenuState::update(double timeSinceLastFrame)
{
    m_FrameEvent.timeSinceLastFrame = timeSinceLastFrame;
    OgreFramework::getSingletonPtr()->m_pTrayMgr->frameRenderingQueued(m_FrameEvent);

    if(m_bQuit == true)
    {
        shutdown();
        return;
    }

    _tiempo += timeSinceLastFrame;

    if(_tiempo >= 1.0){
        _segundos += 1;
        _tiempo = 0;
        cout << "t:" << _segundos << endl;
    }
}


void MenuState::buttonHit(OgreBites::Button *button)
{
    if(button->getName() == "Salir"){
        OgreFramework::getSingletonPtr()->m_pTrayMgr->showYesNoDialog("Salir", "Desea Salir?");
        m_bQuestionActive = true;

    }

    else if(button->getName() == "Jugar")
        changeAppState(findByName("Jugar"));

    else if(button->getName() == "Logros")
        pushAppState(findByName("Logros"));

    else if(button->getName() == "Tutorial")
        pushAppState(findByName("Tutorial"));

    else if(button->getName() == "Opciones")
        changeAppState(findByName("Opciones"));

    else if(button->getName() == "Acerca")
        pushAppState(findByName("Acerca"));
}

void MenuState::yesNoDialogClosed(const Ogre::DisplayString& question, bool yesHit)
{
    if(yesHit == true)
        shutdown();
    else
        OgreFramework::getSingletonPtr()->m_pTrayMgr->closeDialog();

    m_bQuestionActive = false;
}

