#include "AcercaState.h"

AcercaState::AcercaState()
{
    m_bQuit = false;
}

void AcercaState::enter()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Entrando en Acerca...");

    m_pSceneMgr = OgreFramework::getSingletonPtr()->m_pRoot->createSceneManager(ST_GENERIC, "OpcionesSceneMgr");
    m_pSceneMgr->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));

    m_pCamera = m_pSceneMgr->createCamera("AcercaCam");
    m_pCamera->setPosition(Vector3(0, 25, -50));
    m_pCamera->lookAt(Vector3(0, 0, 0));
    m_pCamera->setNearClipDistance(1);

    m_pCamera->setAspectRatio(Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualWidth()) /
                              Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualHeight()));

    OgreFramework::getSingletonPtr()->m_pViewport->setCamera(m_pCamera);

//    OgreFramework::getSingletonPtr()->m_pTrayMgr->destroyAllWidgets();

    OgreFramework::getSingletonPtr()->m_pTrayMgr->showCursor();

    this->_overlayManager = Ogre::OverlayManager::getSingletonPtr();
    fondo = _overlayManager->getByName("Fondo");
    _acercaOverlay = _overlayManager->getByName("AcercaOverlay");
    _textoAcerca = _overlayManager->getByName("TextoAcerca");

    fondo->show();
    _acercaOverlay->show();
    _textoAcerca->show();

    cout << "Overlays Acerca creados" << endl;

    createScene();
    m_bQuit = false;
}

void AcercaState::createScene()
{
}

void AcercaState::exit()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Salir de Acerca...");

    m_pSceneMgr->destroyCamera(m_pCamera);
    if(m_pSceneMgr){
        OgreFramework::getSingletonPtr()->m_pRoot->destroySceneManager(m_pSceneMgr);
    }

//    OgreFramework::getSingletonPtr()->m_pTrayMgr->clearAllTrays();
//    OgreFramework::getSingletonPtr()->m_pTrayMgr->destroyAllWidgets();
//    OgreFramework::getSingletonPtr()->m_pTrayMgr->setListener(0);

    fondo->hide();
    _acercaOverlay->hide();
    _textoAcerca->hide();
}

bool AcercaState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_ESCAPE))
    {
        cout << "ESC presionado" << endl;
        this->m_bQuit=true;
    }

    OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);
    return true;
}

bool AcercaState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
}

bool AcercaState::mouseMoved(const OIS::MouseEvent &evt)
{
    if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseMove(evt)) return true;
    return true;
}

bool AcercaState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseDown(evt, id)) return true;
    return true;
}

bool AcercaState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseUp(evt, id)) return true;
    return true;
}

void AcercaState::update(double timeSinceLastFrame)
{
    if(this->m_bQuit){
        popAppState();
//        this->popAllAndPushAppState(findByName("Menu"));

    }
}
