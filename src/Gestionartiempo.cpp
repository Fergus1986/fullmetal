#include "Gestionartiempo.h"

GestionarTiempo::GestionarTiempo(int m, int s)
{
    this->_minuto = m;
    this->_segundo = s;
}

GestionarTiempo::~GestionarTiempo()
{
}

int GestionarTiempo::getMinuto()
{
    return this->_minuto;
}

int GestionarTiempo::getSegundo()
{
    return this->_segundo;
}

void GestionarTiempo::setMinuto(int m)
{
    this->_minuto = m;
}

void GestionarTiempo::setSegundo(int s)
{
    this->_segundo = s;
}

void GestionarTiempo::setContSegundos(int s)
{
     this->_contSegundos = s;
}

int GestionarTiempo::getContSegundos()
{
    return this->_contSegundos;
}

void GestionarTiempo::contarTiempo()
{
    if((this->_segundo == 0)&&(this->_minuto!=0)){
        this->_minuto -= 1;
        this->_segundo=60;
        this->_segundo -= 1;
        this->_contSegundos++;
    }
    else if((this->_segundo == 0)&&(this->_minuto==0)){
        this->_minuto = 0;
        this->_segundo = 0;
     }
    else{
        this->_segundo -= 1;
        this->_contSegundos++;
    }

}


string GestionarTiempo::getTiempo()
{

    stringstream msg;

   msg << "0" <<  this->_minuto << ":";
    if(this->_segundo<10){
         msg << "0";
    }
    msg << this->_segundo;
    return msg.str();
}

bool GestionarTiempo::finTiempo()
{
    return (this->_segundo == 0) && (this->_minuto == 0);
}
