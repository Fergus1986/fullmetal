#include "Enemigo.h"

Enemigo::Enemigo(string nombre,
                 btVector3 posicionInicial,
                 btVector3 posicionFinal,
                 SceneNode* escenario,
                 SceneManager* sceneManager,
                 PhysicsManager *mPhysics)
{
    this->_physics = mPhysics;
    this->_sceneMgr = sceneManager;
    this->_nombre = nombre;

    // Nodo y entidad
    this->_nodo = _sceneMgr->createSceneNode();
    this->_entidad = _sceneMgr->createEntity(_nombre.append(".mesh"));

//    this->_nodo->translate(Ogre::Vector3(posicionInicial.x(), posicionInicial.y(), posicionInicial.z()));


    _nodo->attachObject(_entidad);
    escenario->addChild(this->_nodo);

    // Posiciones
    this->_posicionInicial = posicionInicial;
    this->_posicionFinal = posicionFinal;

    // Fisica
    // Posicion Inicial
    this->_transformacion.setIdentity();
    _transformacion.setOrigin(_posicionInicial);
    // Fisica: Cuerpo fisico
    _colision = new btBoxShape(btVector3(0.3, 0.3, 0.3));
    _cuerpo = _physics->addRigidBody(_transformacion, _colision, 100, this->_nodo);
    this->_cuerpo->setActivationState(DISABLE_DEACTIVATION);
    _estado = PARADO;

}

Enemigo::~Enemigo()
{
    cout << "Destruyendo Enemigo" << endl;
    _sceneMgr->destroyEntity(this->_entidad);
    _sceneMgr->getRootSceneNode()->removeChild(this->_nodo);
    _physics->getDynamicsWorld()->removeRigidBody(_cuerpo);
}

SceneNode *Enemigo::getNodo()
{
    return this->_nodo;
}

btRigidBody *Enemigo::getCuerpo()
{
    return this->_cuerpo;
}

void Enemigo::actualizarPosicion()
{
    _posicionActual = btVector3(_nodo->getPosition().x, _nodo->getPosition().y, _nodo->getPosition().z);

    switch(_estado){

    case PARADO:
        if( fabs (this->_posicionActual.x() - this->_posicionFinal.x()) <= DELTA){
            cout << "posicion Final" << endl;
            _estado = ATRAS;

        }else{
            _estado = ADELANTE;
        }
        break;

    case ADELANTE:
        moverAdelante();
        break;

    case GIRAR:
        girar();
        break;

    case ATRAS:
        moverAtras();
        break;
    }

}

void Enemigo::moverAdelante()
{
    if( fabs (this->_posicionActual.x() - this->_posicionFinal.x()) <= DELTA){
//        cout << "posicion Final" << endl;
        _transformacion.setOrigin(btVector3(_posicionFinal.x(), _posicionActual.y(), _posicionFinal.z()));
        _rotacion = btQuaternion(btVector3(0,1,0),3.1);
        _transformacion.setRotation(_rotacion);
        this->_cuerpo->setWorldTransform(_transformacion);
        _estado = ATRAS;

    }else{
        this->_cuerpo->applyCentralImpulse(btVector3(3,0,0));
    }


}

void Enemigo::girar()
{
}

void Enemigo::moverAtras()
{
    if( fabs (this->_posicionActual.x() - this->_posicionInicial.x()) <= DELTA){
//        cout << "posicion Inicial" << endl;
        _estado = ADELANTE;
        _transformacion.setOrigin(btVector3(_posicionInicial.x(), _posicionActual.y(), _posicionInicial.z()));
        _rotacion = btQuaternion(btVector3(0,1,0),0);
        _transformacion.setRotation(_rotacion);
        this->_cuerpo->setWorldTransform(_transformacion);

    }else{
        this->_cuerpo->applyCentralImpulse(btVector3(-3,0,0));
    }
}

//void Enemigo::destruir()
//{
//    this->_nodo->setVisible(false);
//    this->_cuerpo->setActivationState(DISABLE_SIMULATION);
//}

