#ifndef ESCENARIO_H
#define ESCENARIO_H

#include <vector>
#include <string>
#include <algorithm>

#include <Ogre.h>

#include "PhysicsManager.hpp"
#include "Casilla.h"
#include "Enemigo.h"

#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>

using namespace std;
using namespace Ogre;
using namespace xercesc;

class Escenario
{
public:
    Escenario(string nombre, Ogre::SceneManager* sceneManager, PhysicsManager* mPhysics);
    ~Escenario();

    int getAncho();
    int getAlto();
    void actualizarEnemigos();
    std::vector<Enemigo*> getEnemigos();
    void destruirEnemigo(Enemigo* enemigo);
    Casilla* getObjetivo();
    Casilla* getStart();
    string getAudio();


private:
    string _nombre;

    SceneNode* _nodoEscenario;
    Ogre::SceneManager* _sceneManager;
    PhysicsManager* _mPhysics;

    int _ancho;
    int _alto;
    // Fisica
    btCollisionShape* _colision;
    btRigidBody* _cuerpo;

    // Escenario
    std::vector<Casilla*> _escenario;
    std::map<int, string> _tipoCasillas;
    std::map<int, string>::iterator _itCasillas;
    std::vector<int> _indiceCasillas;

    // Enemigos
    std::vector<Enemigo*> _enemigos;
    std::vector<Enemigo*>::iterator _itEnemigos;
    std::vector<int> _indiceEnemigos;

    // Objetivo
    Casilla* _objetivo;
    Casilla* _salida;

    // Audio
    string _audio;

    void leerArchivo();
    void crearEscenario();
    void crearEnemigos();
    void parsearCasilla(xercesc::DOMNode* casillaNodo);
    void parsearCapa(xercesc::DOMNode* capaNodo);
    void parsearMapa(xercesc::DOMNode* capaMapa);
    void parsearEnemigos(xercesc::DOMNode* capaEnemigos);

};

#endif // ESCENARIO_H
