#include <iostream>

#include "OGRE/Ogre.h"

#include "AdvancedOgreFramework.hpp"
#include "AppStateManager.hpp"
#include "IntroState.h"
#include "MenuState.hpp"
#include "GameState.hpp"
#include "PauseState.hpp"
#include "LogrosState.h"
#include "OpcionesState.h"
#include "TutorialState.h"
#include "AcercaState.h"
#include "GameOverState.h"
#include "EnhorabuenaState.h"


using namespace std;
using namespace Ogre;

int main(int argc, char *argv[])
{
    cout << "-- FullMetal 0.2" << endl;

    AppStateManager*	m_pAppStateManager;

    new OgreFramework();

    if(!OgreFramework::getSingletonPtr()->initOgre("AdvancedOgreFramework", 0, 0))
        return 1;

    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Full Metal iniciado");

    m_pAppStateManager = new AppStateManager();


    IntroState::create(m_pAppStateManager, "Intro");
    MenuState::create(m_pAppStateManager, "Menu");
    GameState::create(m_pAppStateManager, "Jugar");
    PauseState::create(m_pAppStateManager, "Pausa");
    LogrosState::create(m_pAppStateManager, "Logros");
    TutorialState::create(m_pAppStateManager, "Tutorial");
    OpcionesState::create(m_pAppStateManager, "Opciones");
    AcercaState::create(m_pAppStateManager, "Acerca");
    GameOverState::create(m_pAppStateManager, "GameOver");
    EnhorabuenaState::create(m_pAppStateManager, "Enhorabuena");

    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Estados creados");

    m_pAppStateManager->start(m_pAppStateManager->findByName("Intro"));

    cout << "-- Exito" << endl;
    return 0;
}
