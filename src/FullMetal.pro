QT       -= core

QT       -= gui

TARGET = FullMetal
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += AdvancedOgreFramework/
INCLUDEPATH += Physics/
INCLUDEPATH += Character/
INCLUDEPATH += Sound/
INCLUDEPATH += iniParser/
INCLUDEPATH += /usr/local/include/OGRE
INCLUDEPATH += /usr/include/OIS/
INCLUDEPATH += /usr/include/SDL/
INCLUDEPATH += /usr/include/bullet/
INCLUDEPATH += /usr/include/xercesc

#INCLUDEPATH += /usr/include/OGRE/

CONFIG += link_pkgconfig
PKGCONFIG += OGRE OIS bullet sdl

LIBS += -lGL -lstdc++ -lOgreMain -lOIS -lSDL -lSDL_mixer -lxerces-c

#LIBS += -lOgreTerrain

LIBS += -lBulletDynamics
LIBS += -lBulletCollision
LIBS += -lLinearMath


SOURCES += main.cpp

HEADERS += \
    AdvancedOgreFramework/PauseState.hpp \
    AdvancedOgreFramework/MenuState.hpp \
    AdvancedOgreFramework/GameState.hpp \
    AdvancedOgreFramework/AppStateManager.hpp \
    AdvancedOgreFramework/AppState.hpp \
    AdvancedOgreFramework/AdvancedOgreFramework.hpp

SOURCES += \
    AdvancedOgreFramework/PauseState.cpp \
    AdvancedOgreFramework/MenuState.cpp \
    AdvancedOgreFramework/GameState.cpp \
    AdvancedOgreFramework/AppStateManager.cpp \
    AdvancedOgreFramework/AdvancedOgreFramework.cpp

HEADERS += \
    AdvancedOgreFramework/LogrosState.h

SOURCES += \
    AdvancedOgreFramework/LogrosState.cpp

HEADERS += \
    Physics/PhysicsManager.hpp

SOURCES += \
    Physics/PhysicsManager.cpp

HEADERS += \
    Sound/TrackManager.hpp \
    Sound/Track.hpp \
    Sound/SoundFXManager.hpp \
    Sound/SoundFX.hpp

SOURCES += \
    Sound/TrackManager.cpp \
    Sound/Track.cpp \
    Sound/SoundFXManager.cpp \
    Sound/SoundFX.cpp

HEADERS += \
    Escenario.h

SOURCES += \
    Escenario.cpp

HEADERS += \
    Casilla.h

SOURCES += \
    Casilla.cpp

HEADERS += \
    iniParser/iniparser.h \
    iniParser/dictionary.h

SOURCES += \
    iniParser/iniparser.cpp \
    iniParser/dictionary.cpp

HEADERS += \
    Physics/PhysicsDebug.hpp \
    Physics/MotionState.hpp

SOURCES += \
    Physics/PhysicsDebug.cpp \
    Physics/MotionState.cpp

HEADERS += \
    Character/CharacterControllerPhysics.hpp \

SOURCES += \
    Character/CharacterControllerPhysics.cpp \

HEADERS += \
    AdvancedOgreFramework/TutorialState.h

SOURCES += \
    AdvancedOgreFramework/TutorialState.cpp

HEADERS += \
    AdvancedOgreFramework/OpcionesState.h

SOURCES += \
    AdvancedOgreFramework/OpcionesState.cpp

HEADERS += \
    Enemigo.h

SOURCES += \
    Enemigo.cpp

HEADERS += \
    AdvancedOgreFramework/IntroState.h

SOURCES += \
    AdvancedOgreFramework/IntroState.cpp

HEADERS += \
    Jugador.h

SOURCES += \
    Jugador.cpp

HEADERS += \
    AdvancedOgreFramework/AcercaState.h

SOURCES += \
    AdvancedOgreFramework/AcercaState.cpp

HEADERS += \
    AdvancedOgreFramework/GameOverState.h

SOURCES += \
    AdvancedOgreFramework/GameOverState.cpp

HEADERS += \
    AdvancedOgreFramework/EnhorabuenaState.h

SOURCES += \
    AdvancedOgreFramework/EnhorabuenaState.cpp

HEADERS += \
    Gestionartiempo.h

SOURCES += \
    Gestionartiempo.cpp


