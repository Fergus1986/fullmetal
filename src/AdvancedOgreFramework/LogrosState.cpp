#include "LogrosState.h"

LogrosState::LogrosState()
{
    m_bQuit = false;
}

void LogrosState::enter()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Entrando en Logros...");
    OgreFramework::getSingletonPtr()->m_pTrayMgr->showCursor();


    m_pSceneMgr = OgreFramework::getSingletonPtr()->m_pRoot->createSceneManager(ST_GENERIC, "LogrosSceneMgr");
    m_pSceneMgr->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));

    m_pCamera = m_pSceneMgr->createCamera("LogrosCam");
    m_pCamera->setPosition(Vector3(0, 25, -50));
    m_pCamera->lookAt(Vector3(0, 0, 0));
    m_pCamera->setNearClipDistance(1);

    m_pCamera->setAspectRatio(Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualWidth()) /
        Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualHeight()));

    OgreFramework::getSingletonPtr()->m_pViewport->setCamera(m_pCamera);

    this->_overlayManager = Ogre::OverlayManager::getSingletonPtr();
    fondo = _overlayManager->getByName("Fondo");
    fondo->show();

    _logrosOverlay = _overlayManager->getByName("LogrosOverlay");
    this->_textoOverlay=this->_overlayManager->getOverlayElement("ptx");

    _logrosOverlay->show();

    createScene();
    m_bQuit = false;


}

void LogrosState::createScene()
{
    std::stringstream texto;

    _puntuacion = iniparser_load("media/puntuacion.ini");

    if(_puntuacion == NULL){
        cerr << "No se puede cargar la puntuacion" << endl;

    }else{

        int nPuntuaciones = iniparser_getint(_puntuacion, "puntuacion:numero", -1);

        for (int i=1; i<=nPuntuaciones; i++){
            std::stringstream clave;
            clave << "puntuacion:nivel" << i;
            texto << "Nivel" << i << "\t";
            texto << iniparser_getstring(_puntuacion, clave.str().c_str(), NULL) << "\n";
        }

        _texto = texto.str();
    }
}

void LogrosState::exit()
{
     OgreFramework::getSingletonPtr()->m_pLog->logMessage("Salir de Logros...");

     m_pSceneMgr->destroyCamera(m_pCamera);
     if(m_pSceneMgr){
         OgreFramework::getSingletonPtr()->m_pRoot->destroySceneManager(m_pSceneMgr);
     }

     iniparser_freedict(_puntuacion);

//     OgreFramework::getSingletonPtr()->m_pTrayMgr->clearAllTrays();
//     OgreFramework::getSingletonPtr()->m_pTrayMgr->destroyAllWidgets();
//     OgreFramework::getSingletonPtr()->m_pTrayMgr->setListener(0);

     fondo->hide();
     _logrosOverlay->hide();

}

bool LogrosState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_ESCAPE))
    {
        cout << "ESC presionado" << endl;

        this->m_bQuit=true;
    }

    OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);
    return true;

}

bool LogrosState::keyReleased(const OIS::KeyEvent &keyEventRef)
{

}

bool LogrosState::mouseMoved(const OIS::MouseEvent &evt)
{
    if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseMove(evt)) return true;
    return true;
}

bool LogrosState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
}

bool LogrosState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
}

void LogrosState::buttonHit(OgreBites::Button *button)
{
}


void LogrosState::update(double timeSinceLastFrame)
{
    this->_textoOverlay->setCaption(_texto);
    if(this->m_bQuit){
        popAppState();
//        this->popAllAndPushAppState(findByName("Menu"));

    }
}


