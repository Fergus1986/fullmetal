#ifndef OPCIONESSTATE_H
#define OPCIONESSTATE_H

#include <iostream>
#include <string.h>

#include <OGRE/Ogre.h>

#include "AppState.hpp"

#include "iniparser.h"

using namespace std;
using namespace Ogre;

class OpcionesState: public AppState
{
public:
    OpcionesState();

    DECLARE_APPSTATE_CLASS(OpcionesState)

    void enter();
    void createScene();
    void exit();

    bool keyPressed(const OIS::KeyEvent &keyEventRef);
    bool keyReleased(const OIS::KeyEvent &keyEventRef);

    bool mouseMoved(const OIS::MouseEvent &evt);
    bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
    bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

    void buttonHit(OgreBites::Button* button);
    void checkBoxToggled(OgreBites::CheckBox* box);
    void itemSelected(OgreBites::SelectMenu *menu);
    void update(double timeSinceLastFrame);

    void leerOpciones();
    void guardarOpciones();

 private:
    bool m_bQuit;

    // Overlays
    Ogre::OverlayManager* _overlayManager;
    Ogre::Overlay *fondo;
    Ogre::Overlay *_opcionesOverlay;

    // Archivo de opciones
    dictionary * _opciones;

    // Niveles a elegir
    Ogre::StringVector _niveles;

};

#endif // OPCIONESSTATE_H
