#ifndef GAME_STATE_HPP
#define GAME_STATE_HPP

#define GAMMA 1

#include "AppState.hpp"

#include <string>
#include <algorithm>

#include "PhysicsManager.hpp"

#include "TrackManager.hpp"
#include "SoundFXManager.hpp"

#include "Enemigo.h"
#include "Jugador.h"

#include "Escenario.h"
#include "Gestionartiempo.h"


using namespace std;


class GameState : public AppState
{
public:

    GameState();
    DECLARE_APPSTATE_CLASS(GameState)

    // Estructura para la detección de colisiones
    struct ContactSensorCallback: public btCollisionWorld::ContactResultCallback {


        ContactSensorCallback(
                std::vector<Enemigo*> v,
                btRigidBody* rb,
                Jugador* j,
                GameState* gS):btCollisionWorld::ContactResultCallback(),
                                             vec(v),_jugador(j), _gameState(gS),_destino(rb){ }

        std::vector<Enemigo*> vec;
        std::vector<Enemigo*>::iterator it;
        Jugador* _jugador;
        GameState* _gameState;
        btRigidBody* _destino;

        // Metodo de deteccion de colisiones
        virtual btScalar addSingleResult(
                btManifoldPoint& cp,
                const btCollisionObject* colObj0,
                int partId0,
                int index0,
                const btCollisionObject* colObj1,
                int partId1,
                int index1)
        {
            if(colObj0==_jugador->getCCPhysics()->getGhostObject()){
                //  Recorrer enemigos para comprobar colision
                for(it=vec.begin();it!=vec.end();it++){
                    if(colObj1==(*it)->getCuerpo()){
                        _gameState->colisionEnemigo((*it));
                    }else if(colObj1==_destino){
                        this->_gameState->_victoria = true;
                    }
                }
            }

            return 0;
        }
    };

    void enter();
    void createScene();
    void exit();
    bool pause();
    void resume();

    void moveCamera();
    void getInput();
    void ocultarGUI();
    void buildGUI();

    bool keyPressed(const OIS::KeyEvent &keyEventRef);
    bool keyReleased(const OIS::KeyEvent &keyEventRef);

    bool mouseMoved(const OIS::MouseEvent &arg);
    bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
    bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id);

    void itemSelected(OgreBites::SelectMenu* menu);

    void update(double timeSinceLastFrame);

    void crearCallBack();
    void colisionEnemigo(Enemigo* e);
    void dibujarVida();

    void crearExplosion();


    private:

    bool m_bQuit;

    // GUI
    Ogre::OverlayManager* _overlayManager;
    Ogre::Overlay* _vida1Overlay;
    Ogre::Overlay* _vida2Overlay;
    Ogre::Overlay* _vida3Overlay;
    Ogre::Overlay* _tiempoOverlay;
    OverlayElement* _OETiempo;

    // Escenario
    std::vector<string> _niveles;
    string _nivelActual;
    Escenario* _escenario;
    Jugador* _jugador;
    bool _victoria;

    // Explosiones
    bool _explosion;
    SceneNode* _nodoExplosion;
    Entity* _entidadExplosion;

    // Fisica
    PhysicsManager* mPhysics;
    DebugDrawer* mDebugDrawer;
    bool mEnabledPhysicsDebugDraw;
    ContactSensorCallback* callback;

    // Musica
    bool _musicaActivada;
    TrackManager* _trackMgr;
    SoundFXManager* _soundFxMgr;
    string _musicaNivel;
    TrackPtr _musica;
    SoundFXPtr _sonidoExplosion;

    // Tiempo
    double _tiempo;
    int _segundos;
    double _tiempoExplosion;


    double _contTimeSLF;
    GestionarTiempo* _gtime;


    // Camara
    Ogre::Real _camaraX;
    Ogre::Real _camaraZ;
    Ogre::Vector3 _centroCamara;



};


#endif
