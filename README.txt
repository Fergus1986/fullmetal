﻿FullMetal

FullMetal es un juego de tipo Frogger. Hemos implementado un modo de juego tipo time-attack, donde aparece tu personaje y los tanques enemigos. Tu misión es alcanzar el edificio antes de que se acabe el tiempo.

Tienes tres vidas, si te chocas con un tanque, pierdes una vida. Si se acaba el tiempo, pierdes otra vida. Es un juego simple e intuitivo.

COMPILACIÓN E INSTALACIÓN

En el archivo comprimido se encuentra lo siguiente:
- src -> contiene el código fuente, el archivo de configuración del proyecto QtCreator y los archivos *.blend
- bin -> contiene el ejecutable y el makefile

Para realizar una nueva compilación se debe respetar esta jerarquía de carpetas.

Dependendicas: 
OGRE, OIS, SDL_Mixer, Xerces, bullet
Para su compilación se adjunta un Makefile distinto al sugerido durante el curso, ya que, nosotros hemos realizado el proyecto utilizando qmake, que genera automáticamente su propio Makefile.

REPOSITORIO PÚBLICO

https://bitbucket.org/Fergus1986/fullmetal
