#ifndef ENHORABUENASTATE_H
#define ENHORABUENASTATE_H

#include <iostream>

#include "AppState.hpp"

#include "OGRE/Ogre.h"

using namespace Ogre;
using namespace std;

class EnhorabuenaState: public AppState
{
public:
    EnhorabuenaState();
    DECLARE_APPSTATE_CLASS(EnhorabuenaState)

    void enter();
    void createScene();
    void exit();

    bool keyPressed(const OIS::KeyEvent &keyEventRef);
    bool keyReleased(const OIS::KeyEvent &keyEventRef);

    bool mouseMoved(const OIS::MouseEvent &evt);
    bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
    bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

    void buttonHit(OgreBites::Button* button);

    void update(double timeSinceLastFrame);

private:
    bool m_bQuit;
    // Overlays
    Ogre::OverlayManager* _overlayManager;
    Ogre::Overlay *fondo;
    Ogre::Overlay *_enhorabuenaOverlay;
    Ogre::Overlay *_mano;
};

#endif // ENHORABUENASTATE_H
