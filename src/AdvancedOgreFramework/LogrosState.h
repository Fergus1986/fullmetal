#ifndef LOGROSSTATE_H
#define LOGROSSTATE_H

#include <iostream>

#include <OGRE/Ogre.h>

#include "AppState.hpp"
#include "iniParser/iniparser.h"


using namespace std;
using namespace Ogre;

class LogrosState: public AppState
{
public:
    LogrosState();
    DECLARE_APPSTATE_CLASS(LogrosState)

    void enter();
    void createScene();
    void exit();

    bool keyPressed(const OIS::KeyEvent &keyEventRef);
    bool keyReleased(const OIS::KeyEvent &keyEventRef);

    bool mouseMoved(const OIS::MouseEvent &evt);
    bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
    bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

    void buttonHit(OgreBites::Button* button);

    void update(double timeSinceLastFrame);

private:
    bool m_bQuit;

    // Overlays
    Ogre::OverlayManager* _overlayManager;
    Ogre::Overlay *fondo;
    Ogre::Overlay *_logrosOverlay;
    OverlayElement* _textoOverlay;

    // Archivo de puntuacion
    dictionary * _puntuacion;
    string _texto;



};

#endif // LOGROSSTATE_H
