#include "PhysicsManager.hpp"

PhysicsManager::PhysicsManager() {

	mColConfig = new btDefaultCollisionConfiguration;
	mDispatcher = new btCollisionDispatcher(mColConfig);
	mBroadphase = new btDbvtBroadphase;
	mSolver = new btSequentialImpulseConstraintSolver;
	mWorld = new btDiscreteDynamicsWorld(mDispatcher, mBroadphase, mSolver, mColConfig);

	mWorld->setGravity(btVector3(0, -10, 0));
	mRootSceneNode = 0;

    cout << "Fisica creada" << endl;

}

PhysicsManager::~PhysicsManager() {

	for (int i = mWorld->getNumCollisionObjects() - 1; i >= 0; i--) {
		btCollisionObject * obj = mWorld->getCollisionObjectArray()[i];
		btRigidBody * body = btRigidBody::upcast(obj);

		if (body && body->getMotionState())
			delete body->getMotionState();

		mWorld->removeCollisionObject(obj);

		delete obj;
	}

	delete mWorld;
	delete mSolver;
	delete mBroadphase;
	delete mDispatcher;
	delete mColConfig;
}

void PhysicsManager::addStaticPlane(SceneNode * node) {

	btCollisionShape * groundShape = new btStaticPlaneShape(btVector3(0, 1, 0), 50);
	mCollisionShapes.push_back(groundShape);
	btTransform groundTransform;
	groundTransform.setIdentity();
	groundTransform.setOrigin(btVector3(0, -50, 0));

	MotionState * motionState = new MotionState(groundTransform, node);
	btRigidBody::btRigidBodyConstructionInfo rbInfo(0, motionState, groundShape, btVector3(0, 0, 0));
	btRigidBody * body = new btRigidBody(rbInfo);

	mWorld->addRigidBody(body);
}

btRigidBody* PhysicsManager::addDynamicBox(SceneNode * node, float m) {

	btCollisionShape * colShape = new btBoxShape(btVector3(1, 1, 1));
	mCollisionShapes.push_back(colShape);
	btTransform boxTransform;
	boxTransform.setIdentity();

	btScalar mass(m);
	btVector3 localInertia(0, 0, 0);

	colShape->calculateLocalInertia(mass, localInertia);

	boxTransform.setOrigin(btVector3(node->getPosition().x, node->getPosition().y, node->getPosition().z));

	MotionState * motionState = new MotionState(boxTransform, node);
	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, motionState, colShape, localInertia);
	btRigidBody * body = new btRigidBody(rbInfo);

	mWorld->addRigidBody(body);

	return body;
}

btRigidBody* PhysicsManager::addRigidBody(btTransform transform, btCollisionShape * shape, btScalar mass, SceneNode * node){

	mCollisionShapes.push_back(shape);
	btVector3 localInertia(0, 0, 0);

	shape->calculateLocalInertia(mass, localInertia);
	MotionState * motionState = new MotionState(transform, node);
	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, motionState, shape, localInertia);
	btRigidBody * body = new btRigidBody(rbInfo);

	mWorld->addRigidBody(body);

	return body;
}

void PhysicsManager::addCollisionShape(btCollisionShape * colShape)	{
	mCollisionShapes.push_back(colShape);

}

btDiscreteDynamicsWorld* PhysicsManager::getDynamicsWorld() {
	return mWorld;
}

btCollisionWorld* PhysicsManager::getCollisionWorld() {
	return mWorld->getCollisionWorld();
}

btBroadphaseInterface* PhysicsManager::getBroadphase(){
			return mBroadphase;
}

void PhysicsManager::setRootSceneNode(SceneNode * node) {
	mRootSceneNode = node;
}

btVector3 PhysicsManager::toBullet(const Vector3 & vec) const {
	return btVector3(vec.x, vec.y, vec.z);
}

void PhysicsManager::shootBox(const Vector3 & camPosition) {

	if (mRootSceneNode) {
		SceneNode * node = mRootSceneNode->createChildSceneNode(camPosition);
		btRigidBody * box = addDynamicBox(node);
		box->applyCentralImpulse(btVector3(0, 0, -50));
	}
}
