#include "TutorialState.h"

TutorialState::TutorialState()
{
        m_bQuit = false;
}

void TutorialState::enter()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Entrando en Tutorial...");
    OgreFramework::getSingletonPtr()->m_pTrayMgr->showCursor();


    m_pSceneMgr = OgreFramework::getSingletonPtr()->m_pRoot->createSceneManager(ST_GENERIC, "TutoralSceneMgr");
    m_pSceneMgr->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));

    m_pCamera = m_pSceneMgr->createCamera("TutorialCam");
    m_pCamera->setPosition(Vector3(0, 25, -50));
    m_pCamera->lookAt(Vector3(0, 0, 0));
    m_pCamera->setNearClipDistance(1);

    m_pCamera->setAspectRatio(Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualWidth()) /
        Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualHeight()));

    OgreFramework::getSingletonPtr()->m_pViewport->setCamera(m_pCamera);

    this->_overlayManager = Ogre::OverlayManager::getSingletonPtr();
    fondo = _overlayManager->getByName("Fondo");
    _tutorialOverlay = _overlayManager->getByName("TutorialOverlay");
    _textoTutorial = _overlayManager->getByName("TextoTutorial");

    fondo->show();
    _tutorialOverlay->show();
    _textoTutorial->show();

    cout << "Overlays Opciones creados" << endl;

    createScene();
    m_bQuit = false;
}

void TutorialState::createScene()
{
}

void TutorialState::exit()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Salir de Tutorial...");

    m_pSceneMgr->destroyCamera(m_pCamera);
    if(m_pSceneMgr){
        OgreFramework::getSingletonPtr()->m_pRoot->destroySceneManager(m_pSceneMgr);
    }

//    OgreFramework::getSingletonPtr()->m_pTrayMgr->clearAllTrays();
//    OgreFramework::getSingletonPtr()->m_pTrayMgr->destroyAllWidgets();
//    OgreFramework::getSingletonPtr()->m_pTrayMgr->setListener(0);

    fondo->hide();
    _tutorialOverlay->hide();
    _textoTutorial->hide();
}

bool TutorialState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_ESCAPE))
    {
        cout << "ESC presionado" << endl;
        this->m_bQuit=true;
    }

    OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);
    return true;
}

bool TutorialState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
}

bool TutorialState::mouseMoved(const OIS::MouseEvent &evt)
{
    if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseMove(evt)) return true;
    return true;
}

bool TutorialState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
}

bool TutorialState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
}

void TutorialState::buttonHit(OgreBites::Button *button)
{
}

void TutorialState::update(double timeSinceLastFrame)
{
    if(this->m_bQuit){
        popAppState();
//        this->popAllAndPushAppState(findByName("Menu"));

    }
}
