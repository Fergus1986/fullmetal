#ifndef TUTORIALSTATE_H
#define TUTORIALSTATE_H

#include <iostream>

#include <OGRE/Ogre.h>

#include "AppState.hpp"


using namespace std;
using namespace Ogre;



class TutorialState: public AppState
{
public:
    TutorialState();
    DECLARE_APPSTATE_CLASS(TutorialState)

    void enter();
    void createScene();
    void exit();

    bool keyPressed(const OIS::KeyEvent &keyEventRef);
    bool keyReleased(const OIS::KeyEvent &keyEventRef);

    bool mouseMoved(const OIS::MouseEvent &evt);
    bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
    bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

    void buttonHit(OgreBites::Button* button);

    void update(double timeSinceLastFrame);

private:
    bool m_bQuit;

    // Overlays
    Ogre::OverlayManager* _overlayManager;
    Ogre::Overlay *fondo;
    Ogre::Overlay *_tutorialOverlay;
    Ogre::Overlay *_textoTutorial;

};

#endif // TUTORIALSTATE_H
